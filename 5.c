#include <stdio.h>
int main()
{
    int N, i,z;
    printf("Enter an Integer Number -  ");
    scanf("%d", &N);

    for (z = 1; z <= N; z=z+1) {

                printf("Multiplication Table of %d \n \n", z);

        for (i = 1; i <= 12; i=i+1)
            {
                printf("%d * %d = %d \n", z, i, (z * i) );
            }
            printf("\n");
    }
    return 0;
}
